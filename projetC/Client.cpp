#include <iostream>
#include <cstring>
using namespace std;
#include "Client.h"

Client::Client(char n[51], char p[51], char s, char t[20],
               string l, string com, int cp, string v, int nc)
{
    strcpy(nom, n);
    strcpy(prenom, p);
    this->Setsexe(s);
    strcpy(telephone, t);
    this->Setlibelle(l);
    this->Setcomplement(com);
    this->Setcode_postal(cp);
    this->Setville(v);
    this->Setno_client(nc);
}

Client::~Client()
{
    cout << "Destruction de Client " << this->Getnom() << endl;
}

void Client::affiche()
{
    cout.width(5);
    cout.fill('0');
    cout << Getno_client() << endl << endl;
    cout << "\t";
    if(sexe == 'M')
        cout << "M. ";
    else
        cout << "Mme. ";
    cout << nom << " " << prenom << endl;
    cout << "\t" << Getlibelle() << " " << Getcomplement() << endl;
    cout << "\t" << Getcode_postal() << " " << Getville() << endl << endl;
    cout << "\t" << "Telephone : " << telephone << endl;
}
