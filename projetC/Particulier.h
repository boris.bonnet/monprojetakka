#ifndef PARTICULIER_H
#define PARTICULIER_H

#include "Client.h"

class Particulier : virtual public Client
{
    private:
        char date_de_naissance[7];
        char c_situation_familliale;
        string s_situation_familliale;

    public:
        Particulier(char[51]="\0", char[51]="\0", char='0', char[20]="\0",
               string="\0", string="\0", int=0, string="\0", int=0,
               char='0', char[7]="\0");
        virtual ~Particulier();

        char Getdate_de_naissance() { return date_de_naissance[7]; }
        void Setdate_de_naissance(char val) { date_de_naissance[7] = val; }

        string getSitu() { return s_situation_familliale; };
        void setSitu(char);

        virtual void infos() override;

};

#endif // PARTICULIER_H
