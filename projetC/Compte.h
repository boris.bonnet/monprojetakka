#ifndef COMPTE_H
#define COMPTE_H
#include <cstring>
#include "Particulier.h"
#include "Professionnel.h"

class Compte : public Particulier, public Professionnel
{
    private:
        char no_compte[20];
        char date_ouver[7];
        int solde;
        int montant_decou;

        char date_operation[9];
        int code_operation;
        int montant_operation;
        string libelle_operation="\0";

     public:
        Compte(char[51]="\0", char[51]="\0", char='0', char[20]="\0", string="\0", string="\0", int=0, string="\0", int=0,
               char='0', char[7]="\0",
               long int=0, char[51]="\0", int=0, string="\0",
               char[20]="\0", char[7]="\0", int=0, int=0, int=0, int=0, char[9]="\0");

        virtual ~Compte();

        char Getno_compte() { return no_compte[20]; }
        void Setno_compte(char val) { no_compte[20] = val; }
        char Getdate_ouver() { return date_ouver[7]; }
        void Setdate_ouver(char val) { date_ouver[7] = val; }
        int Getsolde() { return solde; }
        void Setsolde(int val) { solde = val; }
        int Getmontant_decou() { return montant_decou; }
        void Setmontant_decou(int val) { montant_decou = val; }

        char Getdate_operation() { return date_operation[9]; }
        void Setdate_operation(char val) { date_operation[9] = val; }
        int Getcode_operation() { return code_operation; }
        string Setcode_operation(int val); // { code_operation = val; }
        int Getmontant_operation() { return montant_operation; }
        void Setmontant_operation(int val) { montant_operation = val; }

        virtual void infos() override;


};

#endif // COMPTE_H
