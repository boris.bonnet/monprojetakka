#include <iostream>
#include "Gestion.h"
#include "Particulier.h"
#include "Professionnel.h"
#include "Compte.h"

using namespace std;

int main()
{

    Gestion bank("BOURSO");

   /* bank.ajouter(new Particulier("Ducobu", "George", 'M', "0788112233",
                    "10", "rue de la republique", 94000, "Creteil", 125,
                    'M', "020580"));*/
    /*bank.ajouter(new Particulier("SAPIN", "Klaus", 'M', "0155112233",
                    "25", "rue pole nord", 88, "Groenland", 126,
                    'A', "251225"));//,0,"\0",0,"\0","124","030620",50000))
    bank.ajouter(new Professionnel());
*/

    bank.ajouterOperation(new Compte("Ducobu", "George", 'M', "0788112233",
                    "10", "rue de la republique", 94000, "Creteil", 125,
                    'M', "020580",0,"\0",0,"\0",
                    "1000","030620",2000,500,1,100,"20200623"));

/*    bank.ajouterOperation(new Compte("Ducobu", "Julie", 'F', "01022486663",
                    "20", "rue de la paix", 75001, "Paris", 127,
                    'F', "020580",'021154667799', "KONAMI", 2015, "Torte.Ducobu@gmail.com",
                    "1002","030620",4000,600,2,300,"20200623"));*/

    bank.ajouterOperation(new Compte ("SAPIN", "Klaus", 'M', "0155112233",
                    "25", "rue pole nord", 88, "Groenland", 126,
                    'A', "251225",0,"\0",0,"\0",
                    "1004","251200",1000000,111111,3,10000,"20200623"));

    bank.listerOperations();

    bank.listerClient(0);
    bank.listerClient(1);
    bank.listerClient(2);
    bank.supprimer(2);

    bank.listerClient(2);

    bank.supprimer(2);


    bank.listerTout();

    return 0;
}
