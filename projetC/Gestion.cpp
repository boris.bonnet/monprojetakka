#include "Gestion.h"
#include <typeinfo>

Gestion::Gestion(string n)
{
    this->nom = n;
}

Gestion::~Gestion()
{
    map<int, Compte*>::iterator it ;
    for(it = vecCompte.begin(); it != vecCompte.end(); it++)
    {
        delete vecCompte.at(it->first);
    }
    cout << "Destruction Compte " << endl;

    for(int i=0; i < vecClient.size() ; i++)
    {
        delete vecClient[i];
    }
    cout << "Destruction Gestion " << this->nom << endl;

}

void Gestion::listerClient(int posARechercher)
{
    try
    {
        auto c= vecClient.at(posARechercher);
        c->infos();
    }
    catch(exception& ex)
    {
        cout << endl << "Position Introuvable " << ex.what() << endl ;
    }
}

void Gestion::listerTout()
{
    cout << endl << "Gestion : " << Getnom() << " Listing clients " << " - ";
    cout << "Nb Client(s) : " << this->getEffectif() << " client(s)" << endl << endl;
    for(auto elm : vecClient)
    {
        cout << "Nom : " << typeid(elm).name() << endl;
        elm->infos();
        cout << endl << "---------------------------------------" << endl;
    }

}

void Gestion::ajouter(Client* c)
{
    vecClient.push_back(c);
}

void Gestion::supprimer(int posASupprimer)
{
    if (posASupprimer < vecClient.size())
    {
        vecClient.erase(vecClient.begin() + posASupprimer);
        cout << endl << "Client position : " << posASupprimer << " supprimee ! " << endl;
    }
    else
        cout << endl << "Aucun element ne correspond a cette position : " << posASupprimer << endl;
}

void Gestion::modifier(Client*)
{

}

int Gestion::getEffectif()
{
   return vecClient.size();
}


void Gestion::listerOperations()
{
    if (vecCompte.size() > 0)
    {
        cout << "Operation bancaires realisees : " << endl;
        map<int, Compte*>::iterator it ;
        for(it = vecCompte.begin(); it != vecCompte.end(); it++)
        {
            it->second->infos();
        }
    }
    else
    {
        cout << "Compte Vide !!!!" << endl;
    }cout << endl << "****************************************" << endl;

}

void Gestion::ajouterOperation(Compte* pCompte)
{
     if (vecCompte.count(pCompte->Getno_compte()) != 0)
     {
        cout << "Reference numero compte deja existante !!" << endl;
     }
     else
     {
        vecCompte.insert(std::pair<int,Compte*>(pCompte->Getno_compte(),pCompte));
     }
}

void Gestion::supprimerOperation(int)
{

}

void Gestion::modifierOperation(Compte*)
{

}
