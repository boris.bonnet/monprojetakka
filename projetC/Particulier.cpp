#include <cstring>
#include "Particulier.h"

Particulier::Particulier(char n[51], char p[51], char s, char t[20],
               string l, string com, int cp, string v, int nc,
               char sf, char d[7])
               :Client(n, p, s, t, l, com, cp, v, nc)
{
    this->setSitu(sf);
    strcpy(date_de_naissance, d);
}

Particulier::~Particulier()
{
    cout << "Destruction de Particulier " << this->Getnom() << endl;
}

void Particulier::setSitu(char situ)
{
    if (situ == 'C')        s_situation_familliale = "Celibataire";
    else
        if (situ == 'M')        s_situation_familliale = "Marie(e)";
        else
            if (situ == 'D')        s_situation_familliale = "Divorce(e)";
            else
            s_situation_familliale = "Autre";
}

void Particulier::infos()
{
    cout << endl << "Particulier : ";
    Client::affiche();
    cout << "\t" << "Situation familliale : " << getSitu() << endl;
    cout << "\t" << "Date de naissance : (JJMMAA) " << date_de_naissance << endl;
}
