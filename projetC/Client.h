#ifndef CLIENT_H
#define CLIENT_H

#include <iostream>

using namespace std;

class Client
{
    private:
        char nom[51];
        char prenom[51];
        char sexe;
        char telephone[20];
        string libelle;
        string complement;
        int code_postal;
        string ville;
        int no_client;

    public:
        Client(char[51]="\0", char[51]="\0", char='0', char[20]="\0",
               string="\0", string="\0", int=0, string="\0", int=0);
        virtual ~Client();

        char Getnom() { return nom[51]; }
        void Setnom(char val) { nom[51] = val; }
        char Getprenom() { return prenom[51]; }
        void Setprenom(char val) { prenom[51] = val; }
        char Getsexe() { return sexe; }
        void Setsexe(char val) { sexe = val; }
        int Gettelephone() { return telephone[20]; }
        void Settelephone(int val) { telephone[20] = val; }
        string Getlibelle() { return libelle; }
        void Setlibelle(string val) { libelle = val; }
        string Getcomplement() { return complement; }
        void Setcomplement(string val) { complement = val; }
        int Getcode_postal() { return code_postal; }
        void Setcode_postal(int val) { code_postal = val; }
        string Getville() { return ville; }
        void Setville(string val) { ville = val; }
        int Getno_client() { return no_client; }
        void Setno_client(int val) { no_client = val; }

        void affiche();
        virtual void infos() =0;



};

#endif // CLIENT_H
