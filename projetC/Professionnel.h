#ifndef PROFESSIONNEL_H
#define PROFESSIONNEL_H

#include "Client.h"


class Professionnel : virtual public Client
{
    private:
        long int siret;
        char raison_social[51];
        int annee_creation;
        string mail;

    public:
        Professionnel(char[51]="\0", char[51]="\0", char='0', char[20]="\0",
               string="\0", string="\0", int=0, string="\0", int=0,
               long int=0, char[51]="\0", int=0, string="\0");
        virtual ~Professionnel();

        long int Getsiret() { return siret; }
        void Setsiret(long int val) { siret = val; }
        char Getraison_social() { return raison_social[51]; }
        void Setraison_social(char val) { raison_social[51] = val; }
        int Getannee_creation() { annee_creation=2020-annee_creation; return annee_creation; }
        void Setannee_creation(int val) { annee_creation = val; }
        string Getmail() { return mail; }
        void Setmail(string val) { mail = val; }

        virtual void infos() override;
};

#endif // PROFESSIONNEL_H
