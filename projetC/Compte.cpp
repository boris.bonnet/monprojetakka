#include "Compte.h"

Compte::Compte(char n[51], char p[51], char s, char t[20], string l, string com, int cp, string v, int nc,
               char sf, char d[7],
               long int si, char rs[51], int a, string m,
               char no[20], char dc[7], int so, int md, int co, int mo, char dop[9])
               :Client(n, p, s, t, l, com, cp, v, nc),
               Particulier(n, p, s, t, l, com, cp, v, nc, sf, d),
               Professionnel(n, p, s, t, l, com, cp, v, nc, si, rs, a, m)
{
    strcpy(no_compte, no);
    strcpy(date_ouver, dc);
    this->Setsolde(so);
    this->Setmontant_decou(md);

    strcpy(date_operation, dop);
    this->Setcode_operation(co);
    this->Setmontant_operation(mo);
}

Compte::~Compte()
{
    cout << endl << "Destruction Compte : " << no_compte << endl;
}

void Compte::infos()
{
    /*this->affiche();
    cout << "Numero de compte : " << no_compte << endl;
    cout << "Date ouverture compte : (JJMMAA) " << date_ouver << endl;
    cout << "Montant decouvert autorise : " << montant_decou << " Euros" << endl;*/
    cout << no_compte << ";" << date_operation << ";" << Getcode_operation()
    << ";" << Getmontant_operation() << endl;

}


string Compte::Setcode_operation(int val)
{
    switch(val)
    {
        case 1:
            libelle_operation= "Retrait ";
            break;
        case 2:
            libelle_operation= "Carte Bleue ";
            break;
        case 3:
            libelle_operation= "Depot ";
            break;
        default:
            libelle_operation= "Erreur";
            break;
    }
    code_operation = val;

}
