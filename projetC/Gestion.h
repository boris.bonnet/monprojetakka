#ifndef Gestion_H
#define Gestion_H

#include <iostream>
#include <vector>
#include <map>
#include "Client.h"
#include "Compte.h"

using namespace std;

class Gestion
{
    private:
        vector<Client*> vecClient;
        string nom;
        map<int, Compte*> vecCompte;

    public:
        Gestion(string);
        ~Gestion();

        string Getnom() { return nom; }
        void Setnom(string val) { nom = val; }

        void listerClient(int=0);
        void listerTout();
        void ajouter(Client *);
        void supprimer(int=0);
        void modifier(Client *);
        int getEffectif();

        void listerOperations();
        void ajouterOperation(Compte *);
        void supprimerOperation(int);
        void modifierOperation(Compte *);


};

#endif // Gestion_H
