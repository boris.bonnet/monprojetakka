#include <cstring>
#include "Professionnel.h"
#include <iomanip>

Professionnel::Professionnel(char n[51], char p[51], char s, char t[20],
               string l, string com, int cp, string v, int nc,
               long int si, char rs[51], int a, string m)
                              :Client(n, p, s, t, l, com, cp, v, nc)
{
    this->Setsiret(si);
    strcpy(raison_social, rs);
    this->Setannee_creation(a);
    this->Setmail(m);
}

Professionnel::~Professionnel()
{
    cout << "Destruction de Professionnel " << this->Getnom() << endl;
}
void Professionnel::infos()
{
    cout << endl << "Professionnel : ";
    Client::affiche();
    cout << "\t" << "Siret : " << Getsiret() << endl;
    cout << "\t" << raison_social << endl;
    cout << "\t" << "Anciennete : " << Getannee_creation() << " an(s)" << endl;
    cout << "\t" << "Mail : " << Getmail() << endl;
}
